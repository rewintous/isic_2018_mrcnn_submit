"""
Mask R-CNN Lesion Attributes Detector
Based on https://github.com/matterport/Mask_RCNN/blob/master/samples/nucleus/nucleus.py
Note, for ISIC 2018 challenge, only COCO initial weights were used

Usage:
    # Train a new model starting from ImageNet weights
    python3 task2.py train --dataset=/path/to/dataset --truthset=/path/to/truthsubset --subset=train --weights=<weights file|coco|imagenet>

    # Resume training a model
    python3 task2.py train --dataset=/path/to/dataset --truthset=/path/to/truthsubset --subset=train --weights=last

    # Validate model for validation subset, if truthset is specified then iou_metric.csv will contain IoU between ground truth and predicted masks
    python3 task2.py detect --dataset=/path/to/dataset --truthset=/path/to/truthsubset --subset=val --weights=<last or /path/to/weights.h5>

    # Generate predictions for the specified dataset
    python3 task2.py detect --dataset=/path/to/dataset --subset=all --weights=<last or /path/to/weights.h5>
"""
import math
import os

import numpy as np
import skimage.io
from mrcnn import visualize, utils

from skin_common import EnvironmentConfig
from task1 import Task1TrainConfig, Task1SkinDataset, Task1Solution, Task1Model


class Task2TrainConfig(Task1TrainConfig):
    TRAIN_EPOCHS_HEADS = 40
    TRAIN_EPOCHS_ALL = 80
    NUM_CLASSES = 1 + 5


class Task2InferenceConfig(Task2TrainConfig):
    IMAGES_PER_GPU = 1
    RPN_NMS_THRESHOLD = 0.7


class Task2SkinDataset(Task1SkinDataset):

    def setup_classes(self):
        self.add_class("skinroi", 1, "globules")
        self.add_class("skinroi", 2, "milia_like_cyst")
        self.add_class("skinroi", 3, "negative_network")
        self.add_class("skinroi", 4, "pigment_network")
        self.add_class("skinroi", 5, "streaks")

    def format_mask_name(self, class_id, info):
        return info['id'] + "_attribute_" + self.class_names[class_id] + ".png"

    def load_mask(self, image_id):
        """
        Different from Task1 - as 5 classes are there
        :param image_id:
        :return:
        """
        info = self.image_info[image_id]
        masks = []
        for class_id in range(1,self.num_classes):
            mask_path = os.path.join(str(self.ground_truth_dir), info['id'] + "_attribute_" + self.class_names[class_id]+ ".png")
            # Read mask files from .png image
            masks.append(skimage.io.imread(mask_path).astype(np.bool))
        mask = np.stack(masks, axis=-1)
        # Return mask, and array of class IDs of each instance. Since we have
        # one class ID, we return an array of ones
        return mask, np.asarray(range(1,self.num_classes))


class Task2Model(Task1Model):

    def _process_result(self, image_id, result, image):
        iou = np.zeros((self._dataset.num_classes - 1)).astype(np.float32)
        bw_images = []
        if self.truth_available:
            truth_masks, _ = self._dataset.load_mask(image_id)
        # bw_images = np.array(shape = (dataset.num_classes, image.shape[0], image.shape[1], image.shape[2]))
        for class_id in range(1, self._dataset.num_classes):
            bw_images.append(np.zeros(image.shape))

        for idx, val in enumerate(result['class_ids']):
            visualize.apply_mask(bw_images[val - 1], result['masks'][:,:,idx], color=[1, 1, 1], alpha=1.0)
            if self.truth_available:
                iou_this = utils.compute_overlaps_masks(result['masks'][:, :, idx:idx + 1],
                                                        truth_masks[:, :, val - 1:val]).reshape(-1)[0]
                # use Jaccard instead of iou (i.e. if both masks are zero)
                iou[val - 1] = 0 if math.isnan(iou_this) else iou_this

        for class_id in range(1, self._dataset.num_classes):
            skimage.io.imsave("{}/{}_attribute_{}.png".format(self._result_dir, self._dataset.image_info[image_id]["id"],
                                                              self._dataset.class_names[class_id]),
                              np.mean(bw_images[class_id - 1], -1).astype(np.uint8))

        if self.truth_available:
            self.writer.writerow([image_id] + iou.tolist())
            self.fd.flush()
        self.total_iou = self.total_iou + iou


class Task2Solution(Task1Solution):

    def __init__(self):
        super().__init__(dataset_class=Task2SkinDataset, model_class=Task2Model)

    def get_results_dir(self):
        return "./results_task_2"

    def _instantiate_config(self):
        return EnvironmentConfig(description='Mask R-CNN Lesion Attributes Detector', default_logs="logs_task2/")

    def prepare_configs(self):
        return Task2TrainConfig(), Task2InferenceConfig()


if __name__ == '__main__':
    task2_solution = Task2Solution()
    task2_solution.run()
