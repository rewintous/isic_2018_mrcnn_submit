"""
Mask R-CNN Lesion Disease Classification, for task 3 of ISIC 2018 https://challenge2018.isic-archive.com challenge

This is a hybrid model, it requires pretrained weights of Task1 Model - lesion boundary detector.

The training process for Task 3 looks like this:
    # Input data set is processed using lesion boundary detector from Task 1, a lesion mask is generated for every image
    # Ground truth csv file is used to associate the generated mask with ground truth class
    # Model similar to attribute detector from Task 2 is trained to detect image pixels classes

The detection is similar to Task 2:
    # Masks for each class are predicted for an input image
    # A mask with the biggest area is selected
    # The selected mask's class is considered to be a class prediction for the input image

Usage:
    # Train a new model starting from ImageNet weights
    python3 task3.py train --dataset=/path/to/dataset --subset=train --weights=<weights file|coco|imagenet> --detector_weights </path/to/weights.h5> --truthcsv

    # Resume training a model (in order to regenerate images, delete temp directory)
    python3 task3.py train --dataset=/path/to/dataset --subset=train --weights=last --detector_weights </path/to/weights.h5> --truthcsv </path/to/truth.csv>

    # Validate model for validation subset, if inferred images are available, iou_result.csv fill be updated too
    python3 task3.py detect --dataset=/path/to/dataset --subset=val --weights=<last or /path/to/weights.h5>

    # Generate predictions for the specified dataset
    python3 task3.py detect --dataset=/path/to/dataset --subset=all --weights=<last or /path/to/weights.h5>
"""
import csv
import operator
import os

import numpy as np
import skimage.io
from mrcnn.config import Config
from mrcnn.utils import Dataset

from skin_common import EnvironmentConfig, EpochsAwareConfig
from task1 import Task1TrainConfig, Task1Solution, Task1Model, Task1InferenceConfig, Task1SkinDataset
from task2 import Task2SkinDataset, Task2Model


# hardcoded!!!
VAL_IMAGE_COUNT = 2000


class Task3EnvironmentConfig(EnvironmentConfig):

    def setup_attributes(self, parser, default_logs):
        parser.add_argument("command",
                            metavar="<command>",
                            help="'train' or 'detect'")
        parser.add_argument('--dataset', required=False,
                            metavar="/path/to/dataset/",
                            help='Root directory of the dataset')
        parser.add_argument('--weights', required=True,
                            metavar="/path/to/weights.h5",
                            help="Path to weights .h5 file or 'coco'")
        parser.add_argument('--logs', required=False,
                            default=default_logs,
                            metavar="/path/to/logs/",
                            help='Logs and checkpoints directory (default=logs/)')
        parser.add_argument('--subset', required=False,
                            metavar="Dataset sub-directory",
                            help="Subset of dataset to run prediction on")

        parser.add_argument('--truthcsv', required=False,
                            metavar="/path/to/truthset/csv.file",
                            help='csv file containing disease metadata')
        parser.add_argument('--detector_weights', required=False,
                            metavar=" /path/to/weights.h5",
                            help='task 1 lesion boundaries detector weights')

    def validate_arguments(self, args):
        if args.command == "train":
            assert args.dataset, "Argument --dataset is required for training"
            assert args.truthcsv, "CSV ground truth must be specified"
            assert args.detector_weights, "Detector weights must be specified for lesion boundary detector"
        elif args.command == "detect":
            assert args.subset, "Provide --subset to run prediction on"

class Task3TrainConfig(Task1TrainConfig):
    IMAGES_PER_GPU = 1
    # Number of training and validation steps per epoch
    STEPS_PER_EPOCH = (10015 - VAL_IMAGE_COUNT) // IMAGES_PER_GPU
    VALIDATION_STEPS = max(1, VAL_IMAGE_COUNT // IMAGES_PER_GPU)

    IMAGE_MIN_DIM = 640
    IMAGE_MAX_DIM = 640
    NUM_CLASSES = 1 + 7


class Task3InferenceConfig(Task3TrainConfig):
    IMAGES_PER_GPU = 1
    RPN_NMS_THRESHOLD = 0.7


class Task3SkinDataset(Task2SkinDataset):

    @staticmethod
    def _load_csv_file(truth_csv_filename):
        id_to_class = dict()
        with open(truth_csv_filename, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            # skip header
            header = next(reader)
            for row in reader:
                id = row[0]
                for class_no, flag in enumerate(row):
                    if flag != '0.0':
                        id_to_class[id] = class_no
        return id_to_class

    def __init__(self, class_map=None, truth_csv_filename=None):
        super().__init__(class_map, validation_count=VAL_IMAGE_COUNT)
        if truth_csv_filename:
            self.id_to_class = Task3SkinDataset._load_csv_file(truth_csv_filename)

    def setup_classes(self):
        self.add_class("skinroi", 1, "MEL")
        self.add_class("skinroi", 2, "NV")
        self.add_class("skinroi", 3, "BCC")
        self.add_class("skinroi", 4, "AKIEC")
        self.add_class("skinroi", 5, "BKL")
        self.add_class("skinroi", 6, "DF")
        self.add_class("skinroi", 7, "VASC")

    def load_skin(self, dataset_dir, ground_truth_dir, subset):
        super().load_skin(dataset_dir, ground_truth_dir, subset)

    def load_mask(self, image_id):
        """
        loads lesion mask and ground truth class
        :param image_id:
        :return:
        """
        info = self.image_info[image_id]
        true_class_id = self.id_to_class[info['id']]
        mask_path = os.path.join(str(self.ground_truth_dir),
                                 info['id'] + "_segmentation.png")
        # Read mask files from .png image
        mask = [skimage.io.imread(mask_path).astype(np.bool)]
        mask = np.stack(mask, axis=-1)
        return mask, np.asarray([true_class_id])


class Task3Model(Task2Model):

    def __init__(self, train_config: EpochsAwareConfig, inference_config: Config, dataset: Dataset,
                 val_dataset: Dataset, log_dir, result_dir):
        super().__init__(train_config, inference_config, dataset, val_dataset, log_dir, result_dir)
        self.result_writer = None

    def detect(self, load_weights_callback):
        with open(os.path.join(self._result_dir, "result.csv"), "w+") as out_file:
            self.result_writer = csv.writer(out_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL,
                                     lineterminator='\n')
            self.result_writer.writerow(["image"] + self._dataset.class_names[1:])
            super().detect(load_weights_callback)

    def _process_result(self, image_id, result, image):
        super()._process_result(image_id, result, image)
        if self.result_writer:
            source_id = self._dataset.image_info[image_id]["id"]
            classes_score = dict()
            for idx, val in enumerate(result['class_ids']):
                mask = result['masks'][:, :, idx]
                classes_score[val] = np.count_nonzero(mask)

            row_result = np.zeros((self._dataset.num_classes - 1)).astype(np.float32)
            if len(classes_score) > 0:
                max_class = max(classes_score.items(), key=operator.itemgetter(1))[0]
                # winner takes it all thresholding
                row_result[max_class-1] = 1.0
            self.result_writer.writerow([source_id] + row_result.tolist())

    def train(self, load_weights_callback):
        super().train(load_weights_callback)


class Task3Solution(Task1Solution):

    def __init__(self):
        super().__init__(dataset_class=Task3SkinDataset, model_class=Task3Model)

    def get_results_dir(self):
        return "./results_task_3"

    def _instantiate_config(self):
        return Task3EnvironmentConfig(description='Mask R-CNN Lesion Classifier', default_logs="logs_task3/")

    def _load_task1_pretrained_weights(self, model):
        model.load_weights(self._env_config.detector_weights, by_name=True)

    def prepare_datasets(self):

        task1_inference_logs = os.path.abspath("./task3_input_preprocessed/logs")
        task1_inference_results = os.path.abspath("./task3_input_preprocessed/results")

        if self._env_config.command == "detect":
            dataset = Task3SkinDataset()
            dataset.load_skin(self._env_config.dataset,
                              task1_inference_results if os.path.exists(task1_inference_results) else None,
                              self._env_config.subset)
            dataset.prepare()
            return dataset, None
        elif self._env_config.command == "train":

            # Training is two-phase process
            # First, initialize Task1Model, inference mode only
            if not os.path.exists(task1_inference_results):
                print("Preprocessing input images: ")
                task1_dataset = Task1SkinDataset(validation_count=0)
                task1_dataset.load_skin(self._env_config.dataset, None, "all")
                task1_dataset.prepare()

                os.makedirs(task1_inference_logs)
                os.makedirs(task1_inference_results)

                model1_inference = Task1Model(None, Task1InferenceConfig(), task1_dataset, None, task1_inference_logs,
                                   task1_inference_results)
                model1_inference.detect(self._load_task1_pretrained_weights)

                print("Preprocessing complete...")
            else:
                print("Skipping input data inference as " + str(task1_inference_results) + " already exists. Please delete it to restart input inference")

            print("Starting model training...")
            train_dataset = Task3SkinDataset(truth_csv_filename=self._env_config.truthcsv)
            train_dataset.load_skin(self._env_config.dataset, task1_inference_results, "train")
            train_dataset.prepare()

            test_dataset = Task3SkinDataset(truth_csv_filename=self._env_config.truthcsv)
            test_dataset.load_skin(self._env_config.dataset, task1_inference_results, "val")
            test_dataset.prepare()

            print(len(train_dataset.image_ids))
            print(len(test_dataset.image_ids))

            return train_dataset, test_dataset
        else:
            raise RuntimeError("Unknown command")

    def prepare_configs(self):
        return Task3TrainConfig(), Task3InferenceConfig()


if __name__ == '__main__':
    task3_solution = Task3Solution()
    task3_solution.run()
