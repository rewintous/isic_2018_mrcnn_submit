"""
Mask R-CNN Lesion Boundary Detector
Based on https://github.com/matterport/Mask_RCNN/blob/master/samples/nucleus/nucleus.py
Note, for ISIC 2018 challenge only COCO initial weights were used

Usage:
    # Train a new model starting from ImageNet weights
    python3 task1.py train --dataset=/path/to/dataset --truthset=/path/to/truthsubset --subset=train --weights=<weights file|coco|imagenet>

    # Resume training a model
    python3 task1.py train --dataset=/path/to/dataset --truthset=/path/to/truthsubset --subset=train --weights=last

    # Validate model for validation subset, if truthset is specified then iou_metric.csv will contain IoU between ground truth and predicted masks
    python3 task1.py detect --dataset=/path/to/dataset --truthset=/path/to/truthsubset --subset=val --weights=<last or /path/to/weights.h5>

    # Generate predictions for the specified dataset
    python3 task1.py detect --dataset=/path/to/dataset --subset=all --weights=<last or /path/to/weights.h5>
"""
import csv
import datetime
import os
import random
from pathlib import Path

import numpy as np
import skimage
import skimage.io
from mrcnn import utils, visualize
from mrcnn.config import Config
from mrcnn.utils import Dataset

from skin_common import CommonTaskModel, CommonTaskSolution, EpochsAwareConfig, EnvironmentConfig

# Hardcoded!!!
VAL_IMAGE_COUNT = 300


class Task1TrainConfig(EpochsAwareConfig):
    NAME = "skin"

    # Adjust depending on your GPU memory
    IMAGES_PER_GPU = 2

    # Number of classes (including background)
    NUM_CLASSES = 1 + 1  # Background + nucleus

    # Number of training and validation steps per epoch
    STEPS_PER_EPOCH = (2594 - VAL_IMAGE_COUNT) // IMAGES_PER_GPU
    VALIDATION_STEPS = max(1, VAL_IMAGE_COUNT // IMAGES_PER_GPU)

    DETECTION_MIN_CONFIDENCE = 0

    # Backbone network architecture
    # Supported values are: resnet50, resnet101
    BACKBONE = "resnet50"

    # Input image resizing
    # Random crops of size 512x512 vs SQUARE 768 vs pad64 todo
    IMAGE_RESIZE_MODE = "square"
    IMAGE_MIN_DIM = 768
    IMAGE_MAX_DIM = 768

    # Length of square anchor side in pixels
    RPN_ANCHOR_SCALES = (32, 64, 128, 256, 512)

    # ROIs kept after non-maximum supression (training and inference)
    POST_NMS_ROIS_TRAINING = 1000
    POST_NMS_ROIS_INFERENCE = 2000

    # Non-max suppression threshold to filter RPN proposals.
    # You can increase this during training to generate more propsals.
    RPN_NMS_THRESHOLD = 0.9

    # How many anchors per image to use for RPN training
    RPN_TRAIN_ANCHORS_PER_IMAGE = 64

    # If enabled, resizes instance masks to a smaller size to reduce
    # memory load. Recommended when using high-resolution images.
    USE_MINI_MASK = True
    MINI_MASK_SHAPE = (56, 56)  # (height, width) of the mini-mask

    # Number of ROIs per image to feed to classifier/mask heads
    # The Mask RCNN paper uses 512 but often the RPN doesn't generate
    # enough positive proposals to fill this and keep a positive:negative
    # ratio of 1:3. You can increase the number of proposals by adjusting
    # the RPN NMS threshold.
    TRAIN_ROIS_PER_IMAGE = 128

    # Maximum number of ground truth instances to use in one image
    MAX_GT_INSTANCES = 200

    # Max number of final detections per image
    DETECTION_MAX_INSTANCES = 400


class Task1InferenceConfig(Task1TrainConfig):
    IMAGES_PER_GPU = 1
    RPN_NMS_THRESHOLD = 0.7


class Task1SkinDataset(utils.Dataset):

    def __init__(self, class_map=None, validation_count=VAL_IMAGE_COUNT):
        super().__init__(class_map)
        self.ground_truth_dir = None
        self.validation_count = validation_count

    def load_skin(self, dataset_dir, ground_truth_dir, subset):
        """Load a subset of the dataset.

        dataset_dir: Root directory of the dataset
        ground_truth_dir: Lesion Binary Masks (can be None)
        subset: one of:
                * train: stage1_train excluding validation images
                * val: validation images - for static random seed
                * all: all images from dataset_dir are used
        """
        self.ground_truth_dir = ground_truth_dir
        # Add classes. We have one class.
        self.setup_classes()

        # Static random seed will be used here

        random.seed(42)
        jpg_files = [pth for pth in Path(dataset_dir).iterdir() if pth.suffix == '.jpg']

        training_set = random.sample(jpg_files, max(0, len(jpg_files) - self.validation_count))
        validation_set = list(set(jpg_files) - set(training_set))

        assert subset in ["train", "val", "all"]
        if subset == "val":
            image_paths = validation_set
        elif subset == "train":
            image_paths = training_set
        elif subset == "all":
            image_paths = sorted(jpg_files)
        else:
            image_paths = []

        # Add images
        for image_path in image_paths:
            split_names = os.path.split(str(image_path))
            fn, ext = os.path.splitext(split_names[-1])
            self.add_image(
                "skinroi",
                image_id=fn,
                path=str(image_path))

    def setup_classes(self):
        self.add_class("skinroi", 1, "skinroi")

    def load_mask(self, image_id):
        """Generate instance masks for an image.
       Returns:
        masks: A bool array of shape [height, width, instance count] with
            one mask per instance.
        class_ids: a 1D array of class IDs of the instance masks.
        """

        info = self.image_info[image_id]
        masks = []
        for class_id in range(1,self.num_classes):
            mask_path = os.path.join(str(self.ground_truth_dir), self.format_mask_name(class_id, info))
            # Read mask files from .png image
            masks.append(skimage.io.imread(mask_path).astype(np.bool))
        # todo split input mask into several (connected) instances
        mask = np.stack(masks, axis=-1)
        # Return mask, and array of class IDs of each instance. Since we have
        # one class ID, we return an array of ones
        return mask, np.asarray(range(1,self.num_classes))

    def format_mask_name(self, class_id, info):
        return info['id'] + "_segmentation.png"

    def image_reference(self, image_id):
        """Return the path of the image."""
        info = self.image_info[image_id]
        if info["source"] == "skinroi":
            return info["id"]
        else:
            super(self.__class__, self).image_reference(image_id)


class Task1Model(CommonTaskModel):

    def __init__(self, train_config: EpochsAwareConfig, inference_config: Config, dataset: Dataset,
                 val_dataset: Dataset, log_dir, result_dir):
        super().__init__(train_config, inference_config, dataset, val_dataset, log_dir, result_dir)
        self.fd = None
        self.writer = None
        self.total_iou = None
        self.truth_available = False

    def detect(self, load_weights_callback):
        self.truth_available = not self._dataset.ground_truth_dir is None
        self.total_iou = np.zeros((self._dataset.num_classes - 1)).astype(np.float32)
        self.fd = open(os.path.join(self._result_dir, "iou_metric.csv"), "w")
        self.writer = csv.writer(self.fd, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
        self.writer.writerow(["id"] + self._dataset.class_names[1:])
        self.fd.flush()
        super().detect(load_weights_callback)
        self.writer.writerow(["all_avg"] + (self.total_iou / len(self._dataset.image_ids)).tolist())
        self.fd.flush()
        self.fd.close()

    def _process_result(self, image_id, result, image):
        source_id = self._dataset.image_info[image_id]["id"]
        # Save image with masks [0].reshape(1,-1) to get the very first
        regions_count = result['rois'].shape[0]
        iou = np.zeros((self._dataset.num_classes - 1)).astype(np.float32)
        bw_mask_img = np.zeros(image.shape)
        if regions_count > 0:
            result_mask = result['masks'][:, :, 0]
            if self.truth_available:
                truth_masks, _ = self._dataset.load_mask(image_id)
                iou = utils.compute_overlaps_masks(result['masks'], truth_masks).reshape(-1)
                self.total_iou = self.total_iou + iou
            bw_mask_img = visualize.apply_mask(bw_mask_img, result_mask, color=[1, 1, 1], alpha=1.0)

        if self.truth_available:
            self.writer.writerow([source_id] + iou.tolist())
            self.fd.flush()
        skimage.io.imsave("{}/{}_segmentation.png".format(self._result_dir, self._dataset.image_info[image_id]["id"]),
                          np.mean(bw_mask_img, -1).astype(np.uint8))


class Task1Solution(CommonTaskSolution):

    def __init__(self, dataset_class=Task1SkinDataset, model_class=Task1Model):
        assert dataset_class, "Dataset class must be set"
        assert model_class, "Model class must be set"
        self._dataset_class = dataset_class
        self._model_class = model_class
        super().__init__()

    def _get_prepare_submit_dir(self):
        results_dir = os.path.abspath(self.get_results_dir())
        submit_dir = "submit_{:%Y%m%dT%H%M%S}".format(datetime.datetime.now())
        submit_dir = os.path.join(results_dir, submit_dir)
        os.makedirs(submit_dir)
        return submit_dir

    def get_results_dir(self):
        return "./results_task_1"

    def _instantiate_config(self):
        return EnvironmentConfig(description='Mask R-CNN Lesion Boundary Detector', default_logs="logs_task1/")

    def _instantiate_model(self):
        train_dataset, test_dataset = self.prepare_datasets()
        train_config, config = self.prepare_configs()
        return self._model_class(train_config, config, train_dataset, test_dataset,
                          self._env_config.logs, self._get_prepare_submit_dir())

    def prepare_configs(self):
        train_config = Task1TrainConfig()
        config = Task1InferenceConfig()
        return train_config, config

    def prepare_datasets(self):
        train_dataset = self._dataset_class()
        train_dataset.load_skin(self._env_config.dataset, self._env_config.truthset,
                                self._env_config.subset if self._env_config.subset else "train")
        train_dataset.prepare()

        test_dataset = self._dataset_class()
        # it's either test subset or explicitely specified subset for validation (i.e. all from a specified directory)
        test_dataset.load_skin(self._env_config.dataset, self._env_config.truthset, "val")
        test_dataset.prepare()
        return train_dataset, test_dataset


if __name__ == '__main__':
    task1_solution = Task1Solution()
    task1_solution.run()
