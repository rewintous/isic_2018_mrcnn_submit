# Mask R-CNN Model for Lesion boun]dary detection, attributes extraction, and disease classification (ISIC 2018 Challenge)

This project is a solution to ISIC 2018 Challenge [https://challenge2018.isic-archive.com/](https://challenge2018.isic-archive.com/ "ISIC 2018: Skin Lesion Analysis Towards Melanoma Detection"). It is based on the Mask R-CNN implementation [https://github.com/matterport/Mask_RCNN](https://github.com/matterport/Mask_RCNN).

This is a solution to the following tasks:

- Task 1: Lesion Boundary Segmentation (task1.py)
- Task 2: Lesion Attribute Detection (task2.py)
- Task 3: Lesion Diagnosis (task3.py)

## Prerequisites and installation

### Mask R-CNN 

There are zero dependencies different from the original Mask R-CNN repository. In order to install Mask R-CNN implementation:

    git clone https://github.com/matterport/Mask_RCNN.git
    cd Mask_RCNN
    pip3 install -r requirements.txt

or 

    pip3 install -r requirements.txt --user

to install dependencies for local user only

### Tensorflow GPU support

Tensorflow backend is used by default. GPU is required for training the models. Several options to setup tensorflow-gpu are available: to install it from sources, as pip dependency, or using pre-built python wheel. The current project was tested with Tensorflow 1.7.0/CUDA 9.1/cuDNN 7.1 and the following pre-built wheel: [https://github.com/mind/wheels/releases/download/tf1.7-gpu-cuda91-nomkl/tensorflow-1.7.0-cp35-cp35m-linux_x86_64.whl](https://github.com/mind/wheels/releases/download/tf1.7-gpu-cuda91-nomkl/tensorflow-1.7.0-cp35-cp35m-linux_x86_64.whl "tensorflow-1.7.0-gpu, python 3.5").

### Training Data and Ground Truth

Training and validation data was extracted from the “ISIC 2018: Skin Lesion Analysis Towards Melanoma Detection” grand challenge datasets [1][2].

[1] Noel C. F. Codella, David Gutman, M. Emre Celebi, Brian Helba, Michael A. Marchetti, Stephen W. Dusza, Aadi Kalloo, Konstantinos Liopyris, Nabin Mishra, Harald Kittler, Allan Halpern: “Skin Lesion Analysis Toward Melanoma Detection: A Challenge at the 2017 International Symposium on Biomedical Imaging (ISBI), Hosted by the International Skin Imaging Collaboration (ISIC)”, 2017; arXiv:1710.05006.

[2] Philipp Tschandl, Cliff Rosendahl, Harald Kittler: “The HAM10000 Dataset: A Large Collection of Multi-Source Dermatoscopic Images of Common Pigmented Skin Lesions”, 2018; arXiv:1803.10417.

Training and ground truth data are available at:


|  	| Data	|
| ---	| ---	|
|Task 1	| [https://challenge.kitware.com/#phase/5abcb19a56357d0139260e53](https://challenge.kitware.com/#phase/5abcb19a56357d0139260e53) 	|
|Task 2 | [https://challenge.kitware.com/#phase/5abcbb6256357d0139260e5f](https://challenge.kitware.com/#phase/5abcbb6256357d0139260e5f) 	|
|Task 3	| [https://challenge.kitware.com/#phase/5abcbc6f56357d0139260e66](https://challenge.kitware.com/#phase/5abcbc6f56357d0139260e66) 	|


## Running Training and Detection routines

### Task 1

    # Train a new model starting from ImageNet weights
    python3 task1.py train --dataset=/path/to/dataset --truthset=/path/to/truthsubset --subset=train --weights=<weights file|coco|imagenet>

    # Resume training a model
    python3 task1.py train --dataset=/path/to/dataset --truthset=/path/to/truthsubset --subset=train --weights=last

    # Validate model for validation subset, if truthset is specified then iou_metric.csv will contain IoU between ground truth and predicted masks
    python3 task1.py detect --dataset=/path/to/dataset --truthset=/path/to/truthsubset --subset=val --weights=<last or /path/to/weights.h5>

    # Generate predictions for the specified dataset
    python3 task1.py detect --dataset=/path/to/dataset --subset=all --weights=<last or /path/to/weights.h5> 


### Task 2

    # Train a new model starting from ImageNet weights
    python3 task2.py train --dataset=/path/to/dataset --truthset=/path/to/truthsubset --subset=train --weights=<weights file|coco|imagenet>

    # Resume training a model
    python3 task2.py train --dataset=/path/to/dataset --truthset=/path/to/truthsubset --subset=train --weights=last

    # Validate model for validation subset, if truthset is specified then iou_metric.csv will contain IoU between ground truth and predicted masks
    python3 task2.py detect --dataset=/path/to/dataset --truthset=/path/to/truthsubset --subset=val --weights=<last or /path/to/weights.h5>

    # Generate predictions for the specified dataset
    python3 task2.py detect --dataset=/path/to/dataset --subset=all --weights=<last or /path/to/weights.h5>

### Task 3

The training process for Task 3 looks like this:
    # Input data set is processed using lesion boundary detector from Task 1, a lesion mask is generated for every image
    # Ground truth csv file is used to associate the generated mask with ground truth class
    # Model similar to attribute detector from Task 2 is trained to detect image pixels classes

The detection is similar to Task 2:
    # Masks for each class are predicted for an input image
    # A mask with the biggest area is selected
    # The selected mask's class is considered to be a class prediction for the input image

Usage:

    # Train a new model starting from ImageNet weights
    python3 task3.py train --dataset=/path/to/dataset --subset=train --weights=<weights file|coco|imagenet> --detector_weights </path/to/weights.h5> --truthcsv

    # Resume training a model (in order to regenerate images, delete temp directory)
    python3 task3.py train --dataset=/path/to/dataset --subset=train --weights=last --detector_weights </path/to/weights.h5> --truthcsv </path/to/truth.csv>

    # Validate model for validation subset, if inferred images are available, iou_result.csv fill be updated too
    python3 task3.py detect --dataset=/path/to/dataset --subset=val --weights=<last or /path/to/weights.h5>

    # Generate predictions for the specified dataset
    python3 task3.py detect --dataset=/path/to/dataset --subset=all --weights=<last or /path/to/weights.h5>

 