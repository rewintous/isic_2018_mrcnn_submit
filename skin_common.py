"""
Mask R-CNN Lesion Boundary Detector
Based on https://github.com/matterport/Mask_RCNN/blob/master/samples/nucleus/nucleus.py
Note, for ISIC 2018 challenge only COCO initial weights were used

Usage:
    # Train a new model starting from ImageNet weights
    python3 skin_mask.py train --dataset=/path/to/dataset --truthset=/path/to/truthsubset --subset=train --weights=<weights file|coco|imagenet>

    # Resume training a model
    python3 skin_mask.py train --dataset=/path/to/dataset --truthset=/path/to/truthsubset --subset=train --weights=last

    # Validate model for validation subset, if truthset is specified then iou_metric.csv will contain IoU between ground truth and predicted masks
    python3 skin_mask.py detect --dataset=/path/to/dataset --truthset=/path/to/truthsubset --subset=val --weights=<last or /path/to/weights.h5>

    # Generate predictions for the specified dataset
    python3 skin_mask.py detect --dataset=/path/to/dataset --subset=all --weights=<last or /path/to/weights.h5>
"""

import os
import sys

from imgaug import augmenters as iaa

# Root directory of the project
ROOT_DIR = os.path.abspath("./")

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn.config import Config
from mrcnn import utils
from mrcnn.utils import Dataset
from mrcnn import model as modellib
import argparse
import numpy as np


def softmax(x):
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum()


class EnvironmentConfig:

    def __init__(self, description='',default_logs="logs/"):
        parser = argparse.ArgumentParser(
            description=description)
        self.setup_attributes(parser, default_logs)
        args = parser.parse_args()
        self.validate_arguments(args)
        self.print_arguments(args)
        for key, value in vars(args).items():
            self.__setattr__(key, value)

    def setup_attributes(self, parser, default_logs):
        parser.add_argument("command",
                            metavar="<command>",
                            help="'train' or 'detect'")
        parser.add_argument('--dataset', required=False,
                            metavar="/path/to/dataset/",
                            help='Root directory of the dataset')
        parser.add_argument('--truthset', required=False,
                            metavar="/path/to/truthset/",
                            help='Root directory of the truth (masks) dataset')
        parser.add_argument('--weights', required=True,
                            metavar="/path/to/weights.h5",
                            help="Path to weights .h5 file or 'coco'")
        parser.add_argument('--logs', required=False,
                            default=default_logs,
                            metavar="/path/to/logs/",
                            help='Logs and checkpoints directory (default=logs/)')
        parser.add_argument('--subset', required=False,
                            metavar="Dataset sub-directory",
                            help="Subset of dataset to run prediction on")

    def validate_arguments(self, args):
        # Validate arguments
        if args.command == "train":
            assert args.dataset, "Argument --dataset is required for training"
            assert args.truthset, "Argument --truthset is required for training"
        elif args.command == "detect":
            assert args.subset, "Provide --subset to run prediction on"
            if args.truthset is None:
                print("--truthset is not specified, validation against ground truth is disabled")

    def print_arguments(self, args):
        for key, value in vars(args).items():
            print(key + ": ", value)

class EpochsAwareConfig(Config):
    TRAIN_EPOCHS_HEADS = 20
    TRAIN_EPOCHS_ALL = 40


class CommonTaskModel:

    def __init__(self, train_config: EpochsAwareConfig, inference_config: Config,
                 dataset: Dataset, val_dataset: Dataset,
                 log_dir, result_dir):
        self._train_config = train_config
        self._inference_config = inference_config
        self._dataset = dataset
        self._val_dataset = val_dataset
        self._log_dir = log_dir
        self._result_dir = result_dir

    def train(self, load_weights_callback):
        """Train the model."""

        # Image augmentation
        # http://imgaug.readthedocs.io/en/latest/source/augmenters.html
        augmentation = iaa.SomeOf((0, 2), [
            iaa.Fliplr(0.5),
            iaa.Flipud(0.5),
            iaa.OneOf([iaa.Affine(rotate=90),
                       iaa.Affine(rotate=180),
                       iaa.Affine(rotate=270)]),
            iaa.Multiply((0.8, 1.5)),
            iaa.GaussianBlur(sigma=(0.0, 5.0))
        ])

        model = modellib.MaskRCNN(mode="training", config=self._train_config, model_dir=self._log_dir)
        load_weights_callback(model)

        print("Train network heads")
        model.train(self._dataset, self._val_dataset,
                    learning_rate=self._train_config.LEARNING_RATE,
                    epochs=self._train_config.TRAIN_EPOCHS_HEADS,
                    augmentation=augmentation,
                    layers='heads')

        print("Train all layers")
        model.train(self._dataset, self._val_dataset,
                    learning_rate=self._train_config.LEARNING_RATE,
                    epochs=self._train_config.TRAIN_EPOCHS_ALL,
                    augmentation=augmentation,
                    layers='all')

    def detect(self, load_weights_callback):

        # Create directory
        if not os.path.exists(self._result_dir):
            os.makedirs(self._result_dir)

        model = modellib.MaskRCNN(mode="inference", config=self._inference_config,
                                  model_dir=self._log_dir)
        load_weights_callback(model)

        for image_id in self._dataset.image_ids:
            # Load image and run detection
            image = self._dataset.load_image(image_id)
            # Detect objects
            r = model.detect([image], verbose=0)[0]
            self._process_result(image_id, r, image)

    def _process_result(self, image_id, result, image):
        pass


class CommonTaskSolution:
    # Path to trained weights file
    COCO_WEIGHTS_PATH = os.path.join(ROOT_DIR, "mask_rcnn_coco.h5")

    def __init__(self):
        self._env_config = self._instantiate_config()
        self._task_model = self._instantiate_model()

    def _instantiate_config(self):
        return EnvironmentConfig()

    def _instantiate_model(self):
        raise RuntimeError("_instantiate_model method must be overriden")

    def run(self):
        if self._env_config.command == "train":
            self._task_model._train_config.display()
            self._task_model.train(self.resolve_weights)
        elif self._env_config.command == "detect":
            self._task_model._inference_config.display()
            self._task_model.detect(self.resolve_weights)

    def resolve_weights(self, model):
        weights = self._env_config.weights
        if weights.lower() == "coco":
            weights_path = self.COCO_WEIGHTS_PATH
            # Download weights file
            if not os.path.exists(weights_path):
                utils.download_trained_weights(weights_path)
        elif weights.lower() == "last":
            # Find last trained weights
            weights_path = model.find_last()[1]
        elif weights.lower() == "imagenet":
            # Start from ImageNet trained weights
            weights_path = model.get_imagenet_weights()
        else:
            weights_path = weights
        # Load weights
        print("Loading weights ", weights_path)
        if weights.lower() == "coco":
            # Exclude the last layers because they require a matching
            # number of classes
            model.load_weights(weights_path, by_name=True, exclude=[
                "mrcnn_class_logits", "mrcnn_bbox_fc",
                "mrcnn_bbox", "mrcnn_mask"])
        elif model is not None:
            model.load_weights(weights_path, by_name=True)